#
#    version.py
#
#    (c) Gibbs Consulting, a division of 0802100 (BC) Ltd, 2020
#
#    This file is part of friendly-train.
#
#    Friendly-train is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Friendly-train is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with friendly-train.  If not, see <https://www.gnu.org/licenses/>.
#

__version__ = "0.0.1"
